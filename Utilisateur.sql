CREATE TABLE IF NOT EXISTS `Utilisateur` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Nom` varchar(200) DEFAULT NULL,
  `Prenom` varchar(200) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Date_inscription` date DEFAULT NULL,
  `Tel` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;

INSERT INTO Utilisateur (id, Nom, Prenom, Email, Date_inscription, Tel) VALUES (12,'Rama', 'Soumare', 'rama@neuf.fr',' 2021-02-24',45545454);
-- INSERT INTO Utilisateur (id, Nom, Prenom, Email, Date_inscription, Tel) VALUES (13,'Rama', 'Soumare', 'rama@neuf.fr',' 2021-02-24',45545454);
-- -- INSERT  INTO  Message(id,Titre,Message) values('2','salade césar','INGRÉDIENTS : 4 escalopes de poulet 1 salade verte au choix 100 g de parmesan en copeaux 7 biscottes quelques croutons');