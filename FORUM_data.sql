/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET SQL_NOTES=0 */;
-- DROP DATABASE IF EXISTS FORUM ;
-- CREATE DATABASE /*!32312 IF NOT EXISTS*/ FORUM /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
-- USE promo14_amira_forum;

DROP TABLE IF EXISTS Categorie;
CREATE TABLE IF NOT EXISTS `Categorie` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Libelle` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;

DROP TABLE IF EXISTS Sujet;
CREATE TABLE IF NOT EXISTS  `Sujet` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Titre` varchar(200) DEFAULT NULL,
  `Sous_titre` varchar(200) DEFAULT NULL,
  -- `id_Utilisateur` int DEFAULT NULL,
    PRIMARY KEY (`id`)
  -- KEY `id_Utilisateur` (`id_Utilisateur`),
  -- CONSTRAINT `Sujet_ibfk_1` FOREIGN KEY (`id_Utilisateur`) REFERENCES `Utilisateur` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

DROP TABLE IF EXISTS Utilisateur;
CREATE TABLE IF NOT EXISTS `Utilisateur` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Nom` varchar(200) DEFAULT NULL,
  `Prenom` varchar(200) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Date_inscription` date DEFAULT NULL,
  `Tel` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;

DROP TABLE IF EXISTS Message;

CREATE TABLE IF NOT EXISTS `Message` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Titre` varchar(200) DEFAULT NULL,
  `Message` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

INSERT INTO Categorie(id,Libelle) VALUES(1,'Entrée'),(3,'Plat principal'),(4,'Dessert');


INSERT INTO Utilisateur(Nom,Prenom,Email,Date_inscription,Tel) VALUES('Habi','Amira','amira.simplon@gmail.com','2021-02-24',669123505),('Espeisse','Christopher','christopher.espeisse@gmail.com','2020-04-09',785561209),('Ketfi Cherif','Nasrine','ketfi.nasrine@gmail.com','2020-08-07',643289733),('Sasha','Mimi','sasha;simplon@gmail.com','2018-10-24',667752345),('Ahmed','Moudi','ahmed.simplon@gmail.com','2019-05-29',765341123),('Dahou','Salim','dahou.simplon@gmail.com','2017-03-30',776454323),('Fayzi','Noussa','fayzi.simplon@gmail.com','2020-01-09',743563312),('Linea','Asma','linea.simplon@gmail.com','2021-01-20',456670011),('Haffar','Maroua','maroua.simplon@gmail.com','2020-12-01',654320098),('Derrafa','Marie','marie.simplon@gmail.com','2020-04-15',456765444);


INSERT INTO Sujet (Titre) VALUES('Entrée Froide'),('Entrée Chaude'),('Végétarien'),('Sandwitch'),('Pizza'),('Gâteaux'),('Glaces');
INSERT INTO Message(Titre,message) values('salade césar','INGRÉDIENTS : 4 escalopes de poulet 1 salade verte au choix 100 g de parmesan en copeaux 7 biscottes quelques croutons');
INSERT  INTO  Message(Titre,message) values('Salde de Riz','200 g de riz cuit 250 g de thon au naturel en conserve 2 tomates 1 petite boîte de maïs en conserve 1 poignée dolives vertes dénoyautées 1 échalote 3 c. à soupe dhuile dolive 1 c. à soupe de vinaigre balsamique sel, poivre  ÉPARATION : Préparation 10 min Egouttez et émiettez le thon dans un bol. Nettoyez et coupez les tomates en morceaux. Pelez et hachez finement léchalote. Découpez les olives vertes en morceaux.Mélangez lhuile dolive, le vinaigre balsamique, du sel et du poivre dans un saladier.Ajoutez tout le reste des ingrédients et remuez bien.');
INSERT  INTO  Message(Titre,message) values('salade de chevre','1 salade frisée , 1 bûche de fromage de chèvre, 1 baguette, 1 c. à soupe de vinaigre balsamique, 1 paquet de lardons émincés fumés');
INSERT  INTO  Message(Titre,message) values('Pizza italienne ','500 g de farine (ou un mélange de 250 g de farine normale + 250 g de farine à pain');
INSERT  INTO  Message(Titre,message) values('Pâtes aux courgettes et pancetta','Pâtes 400 g Pancetta 150 g Petites courgettes 3 Jaunes doeufs 3 Crème fraîche épaisse 10 cl Parmesan râpé 60 g Huile dolive 2 cuillères à soupe Sel, poivre');
INSERT INTO  Message(Titre,message) values('Couscous royal','6  Courgettes , 2  Aubergines , 600 g Carottes');
INSERT INTO  Message(Titre,message) values('Tiramisu','100 g Sucre en poudre  200 g Biscuits  30 g Cacao  500 g Mascarpone');