import { Messages } from "../entity/Message";
import { connection } from "./connection";



export class MessageRepository
{
    static async  findAll(){
        const [ rows] = await connection.execute('SELECT * FROM Message');
        const message = [] ;
        for (const row of rows) {
            let instance = new Messages (row.Titre , row.Message, row.id_utilisateur,row.id);
            message.push(instance);
        }
        return message;
    }
    static async add(message) {
        await connection.query ("INSERT INTO Message (Titre, Message) VALUES (?, ?)", [message.titre, message.message]);
    }
    static async update(update) {
        const [rows] = await connection.execute('UPDATE Message SET Libel = ? WHERE id = ?',[update.message,update.titre,update.message]); 
        
    }
        static   async delete(id)
        {
            const [rows] = await connection.execute('DELETE FROM Message WHERE id=?', [id]);
        }
}