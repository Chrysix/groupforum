import { Categorie } from "../entity/Categorie";
import { connection } from "./connection";


export class categorieRepository{
    /**
     * Trouver toutes les lignes de la table categorie
     * @returns 
     */

    static async findAll()
    {
        const [rows] = await connection.query('SELECT * FROM Categorie');
        const cat = [];
        //  console.log(rows);
        for(const row of rows)
        {
        let instance = new Categorie(row.id_Categorie, row.Libelle);
        cat.push(instance);
        }
        return cat;

    };

    /**
     * Trouver une ligne par son identifiant dans la table des categorie
     * @param {*} param 
     * @returns 
     */
    static async findById(param) {
        if (isNaN(param)) {
            return `${param} is not a number`
        } else {
            const [rows] = await connection.query(`SELECT * FROM Categorie WHERE id_categorie=?`, [param]);
            let catById = [];
            for (const row of rows) {
                let instance = new Categorie(row.id_Categorie, row.Libelle)
                catById.push(instance);
            }
            return catById;
        }
    }

    /**
     * ajouter une ligne dans la table categorie
     * @param {*} libel 
     */
    static async Add(libel)
    {
       await connection.query('INSERT INTO Categorie (Libelle) VALUES (?)',[libel]);
    }

    /**
     * mettre à jour une ligne dans la table categorie
     * @param {*} updateCat 
     */
    static async update(updateCat)
    {
       await connection.query('UPDATE Categorie SET Libelle = ? WHERE id_Categorie = ?',[updateCat.libel, updateCat.id_Categorie]); 
    }

    /**
     * supprimer une ligne par son identifiant dans la table categorie
     * @param {*} id 
     */
    static async delete(id)
    {
        await connection.query('DELETE FROM Categorie WHERE id_Categorie=?', [id]);
    }
}