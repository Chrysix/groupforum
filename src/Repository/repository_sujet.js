import { connection } from "./connection";
import { Sujet } from "../entity/Sujet";

export class RepositorySujet {

    static async findAll() {
        const [rows] = await connection.query(`SELECT * FROM Sujet`);
        let sujets = []
        for (const row of rows){
            let instance = new Sujet(row.Sous_titre,row.Titre, row.id);
        sujets.push(instance);
        }
        return sujets
    }

    static async add(sujet) {
        await connection.query(`INSERT INTO Sujet (Titre, Sous_titre) VALUES (?,?)`, [sujet.titre, sujet.subtitle]);
    }

    static async update(up)
    {
        await connection.query('UPDATE Sujet SET Titre = ?, Sous_titre = ? WHERE id = ?',[up.titre, up.subtitle, up.id]);
        
    }

    static async delete(id) {
        await connection.query("DELETE FROM Sujet WHERE id= ?", [id])
    }

    static async findById(id) {
        const [rows] = await connection.query(`SELECT * FROM Sujet WHERE id=?`, [id]);
        if (rows.length === 1) {
            let instance = new Sujet(rows[0].Sous_titre, rows[0].Titre,rows[0].id);
            return instance
        }
        return null;
    }
}
