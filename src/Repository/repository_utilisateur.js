import { Utilisateur } from "../entity/Utilisateur";
import { connection } from "./connection";


export class UtilisateurRepository{
    /**
     * Trouver toutes les lignes de la table utilisateur
     * @returns 
     */

    static async findAll()
    {
        const [rows] = await connection.query('SELECT * FROM Utilisateur');
        const user = [];
        for(const row of rows)
        {
        let instance = new Utilisateur(row.id, row.Nom, row.Prenom, row.Email, row.Date_inscription, row.Tel);
        user.push(instance);
        }
        return user;

    };

    /**
     * Trouver une ligne par son identifiant dans la table utilisateur
     * @param {*} param 
     * @returns 
     */
    static async findById(param) {
            const [rows] = await connection.query(`SELECT * FROM Utilisateur WHERE id=?`, [param]);
            let userById = [];
            for (const row of rows) {
            let instance = new Categorie(row.id, row.Nom, row.Prenom, row.Email)
            userById.push(instance);
            
            return userById;
        }
    }


    /**
     * ajouter une ligne dans la table utilisateur
     * @param {*} utilisateur 
     */
    static async Add(utilisateur)
    {
       await connection.execute('INSERT INTO Utilisateur (Nom, Prenom, Email, Date_inscription, Tel) VALUES (?,?,?,?,?)',[utilisateur.Nom, utilisateur.Prenom, utilisateur.Email, utilisateur.Date_inscription,utilisateur.Tel]);
    }

    /**
     * mettre à jour une ligne dans la table utilisateur
     * @param {*} updateUser 
     */
    static async update(updateUser)
    {
       const [rows] = await connection.execute('UPDATE Utilisateur SET Nom = ? WHERE id = ?',[updateUser.Nom, updateUser.id]); 
    }

    /**
     * supprimer une ligne par son identifiant dans la table utilisateur
     * @param {*} id 
     */
    static async delete(id)
    {
        const [rows] = await connection.execute('DELETE FROM Utilisateur WHERE id=?', [id]);
    }
}