export class Utilisateur
{
     id;
     nom;
     prenom;
     email;
     date_inscription;
     tel;

     constructor (nom, prenom, email, date, tel, id)
     {
         this.nom = nom;
         this.prenom = prenom;
         this.email = email;
         this.Date_inscription = date;
         this.tel = tel;
         this.id = id;
     }
}
