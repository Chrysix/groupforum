export class Categorie 
{
  /**
   * @param {number} id
   * @param {string} libel
   */
    id;
    libel;
    
    constructor(id,libel)
    {
      this.id = id;
      this.libel = libel;
    
    }
}