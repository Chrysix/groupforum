export class Messages {
    titre;
    message;
    id_utilisateur;
    id;
    constructor(titre, message,id_utilisateur, id) {
        this.titre = titre;
        this.message = message;
        this.id_utilisateur = id_utilisateur;
        this.id = id;
    }
}