import { Router } from "express";
import { UtilisateurRepository } from "../Repository/repository_utilisateur";

export const utilisateurController = Router();



utilisateurController.get('/utilisateur', async(req, res) => {
    let data = await UtilisateurRepository.findAll();
    res.json(data);
    res.end();
})

utilisateurController.get('/utilisateur/:id', async (req, res) => {
    let data =  await  UtilisateurRepository.findById(req.params.id);
    res.json(data);
     res.end(); 
});
 
utilisateurController.post('/add', async (req, res)=>{
    
    await  UtilisateurRepository.Add(req.body);
    res.end(); 
});

 utilisateurController.put('/update', async (req, res)=>{
    
    await UtilisateurRepository.update(req.body);
    res.end(); 
});
utilisateurController.delete('/delete/:id', async (req, res)=>{
    
    await UtilisateurRepository.delete(req.body);
    res.end(); 
}); 