import { Router } from "express";
import { connection } from "../Repository/connection";
import { RepositorySujet } from "../Repository/repository_sujet";

export const sujetController = Router();


//Trouver sujet by id
sujetController.get("/sujetid/:id", async (req, resp) => {
    let sujets = await RepositorySujet.findById(req.params.id)
    resp.json(sujets)
})


//Trouver tout les sujets
sujetController.get("/allsujet", async (req, resp) =>{
    const sujets = await RepositorySujet.findAll()
    resp.json(sujets)
})


//Ajouter sujet
sujetController.post("/addsujet", async (req, resp) =>{
    await RepositorySujet.add(req.body)
    resp.end()
})


//Mettre à jour un sujet
sujetController.put("/updatesujet", async (req, resp) =>{
    await RepositorySujet.update(req.body)
    resp.end()
})


//Supprimer un sujet
sujetController.post("/deleteSujet", async (req, resp) =>{
    await RepositorySujet.delete(req.params.body)
    resp.end()
})


// {
 
//     "Titre": "test",
//     "Sous_titre": null
  
//   }