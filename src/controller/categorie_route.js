import { Router } from "express";
import { categorieRepository } from "../Repository/repository_categorie";

export const categorieController = Router();

categorieController.get('/categorie', async(req, res) => {
    try {
       let data = await categorieRepository.findAll();
       res.json(data);
    } catch (error) {
        console.log(error)
        res.end();
    }
   
  })

  
categorieController.get('/categorie/:id', async (req, res) => {
   try {
       let data =  await categorieRepository.findById(req.params.id);
       res.json(data);
   } catch (error) {
       console.log(error)
        res.status(500).end();
       
   }
   
  });

categorieController.post('/add', async (req, res)=>{
   try {
       await categorieRepository.Add(req.body);
   res.end(); 
       
   } catch (error) {
       console.log(error);
       res.end();
       
   }
   
   
});

categorieController.put('/update', async (req, res)=>{
   try {
       await categorieRepository.update(req.body);
       res.end(); 
       
   } catch (error) {
       
       
   }
  
});
categorieController.delete('/delete/:id', async (req, res)=>{
   try {
       await categorieRepository.delete(req.params.id);
       res.end(); 
   } catch (error) {
       console.log(error);
       res.status(500).end();
       
   }

}); 