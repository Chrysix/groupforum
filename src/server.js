import express from 'express';
import cors from 'cors';
import { categorieController } from './controller/categorie_route';
import { utilisateurController } from './controller/utilisateur_route';
import { messagecontreller } from "./controller/routemessage";
import { sujetController } from "./controller/sujet-controller";

export const server = express();

server.use(express.json());
server.use(cors());

server.use('/api/forum/cat',categorieController);
server.use('/api/forum/user',utilisateurController);
server.use('/api/forum/mes',messagecontreller);
server.use('/api/forum/suj',sujetController);