# Forum - MySQL + Repository ( +API REST)

## Projet Forum 

Voilà le projet que nous avons réalisés tout les trois, nous avons crées un forum sur le restaurant et le but de ce projet étais de concevoir et mettre en place la base de données pour une application de forum.

Nous nous somme partagés les taches tout et nous avons commencer a choisir une thématique pour le forum,
lister tout les fonctionalité, créé une User Case pour certaines fonctionnalités,
créé une maquettes, identifié les entités du forum et crées les script.


### Projet: 


https://forum-restaurant.herokuapp.com/

### Maquette:


https://whimsical.com/forum-restaurant-MKurQDvoiFoRhWohqbjZQQ

### User Case:


https://gitlab.com/Chrysix/groupforum/uploads/7e6a695b958ca1cd7ba1995638a36dcd/image.png

### Diagramme de classe:


https://gitlab.com/Chrysix/groupforum/-/issues/3/designs/MCD_FORUM__1_.png