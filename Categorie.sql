CREATE TABLE IF NOT EXISTS `Categorie` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Libelle` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;

INSERT INTO Categorie(id,Libelle) VALUES(1,'Entrée'),(2,'Plat principal'),(3,'Dessert');