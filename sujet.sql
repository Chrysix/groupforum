CREATE TABLE IF NOT EXISTS  `Sujet` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Titre` varchar(200) DEFAULT NULL,
  `Sous_titre` varchar(200) DEFAULT NULL,
  `id_categorie` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categorie` (`id_categorie`),
  CONSTRAINT `Sujet_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `Categorie` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- ALTER TABLE
--         Sujet 
--     ADD 
--         COLUMN `categorie_id` INTEGER, 
--     ADD 
--         FOREIGN KEY (categorie_id) REFERENCES Categorie(id)
--          ON DELETE SET NULL;
INSERT INTO Sujet(id,Titre,Sous_titre) VALUES(1,'Entrée Froide','Salade');
