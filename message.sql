CREATE TABLE `Message` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Titre` varchar(200) DEFAULT NULL,
  `Message` varchar(600) DEFAULT NULL,
  `id_Utilisateur` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3

INSERT INTO Message(id,Titre,Message) values('1','salade césar','INGRÉDIENTS : 4 escalopes de poulet 1 salade verte au choix 100 g de parmesan en copeaux 7 biscottes quelques croutons');
INSERT  INTO  Message(id,Titre,Message) values('2','Salde de Riz','200 g de riz cuit 250 g de thon au naturel en conserve 2 tomates 1 petite boîte de maïs en conserve 1 poignée dolives vertes dénoyautées 1 échalote 3 c. à soupe dhuile dolive 1 c. à soupe de vinaigre balsamique sel, poivre  ÉPARATION : Préparation 10 min Egouttez et émiettez le thon dans un bol. Nettoyez et coupez les tomates en morceaux. Pelez et hachez finement léchalote. Découpez les olives vertes en morceaux.Mélangez l'huile d'olive, le vinaigre balsamique, du sel et du poivre dans un saladier.Ajoutez tout le reste des ingrédients et remuez bien.');
INSERT  INTO  Message(id,Titre,Message) values('3','salade de chevre','1 salade frisée , 1 bûche de fromage de chèvre, 1 baguette, 1 c. à soupe de vinaigre balsamique, 1 paquet de lardons émincés fumés');
INSERT  INTO  Message(id,Titre,Message) values('4','Pizza italienne ','500 g de farine (ou un mélange de 250 g de farine normale + 250 g de farine à pain');
INSERT  INTO  Message(id,Titre,Message) values('5','Pâtes aux courgettes et pancetta','Pâtes 400 g Pancetta 150 g Petites courgettes 3 Jaunes doeufs 3 Crème fraîche épaisse 10 cl Parmesan râpé 60 g Huile dolive 2 cuillères à soupe Sel, poivre');
INSERT INTO  Message(id,Titre,Message) values('6','Couscous royal','6  Courgettes , 2  Aubergines , 600 g Carottes  , ');
INSERT INTO  Message(id,Titre,Message) values('7','Tiramisu','100 g Sucre en poudre  200 g Biscuits  30 g Cacao  500 g Mascarpone');